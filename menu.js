$('#check-box').change(function () {
    if ($(this).is(":checked")) {
        $('.mockups-image').addClass('remove-image');
        $('.menu').removeClass('remove-menu');
    } else {
        $('.mockups-image').removeClass('remove-image');
        $('.menu').addClass('remove-menu');
    }
});